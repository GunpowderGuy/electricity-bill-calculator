extern crate strum;
use strum_macros::{Display, EnumIter};

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Message {
    RadioSelectedA(Habitacion),
    RadioSelected(Electrodomestico),
    Texto(String, usize),
}

impl Message {
    pub fn costo_defecto() -> usize {
        1
    }
}
#[derive(Debug, Clone, Copy, PartialEq, Eq, EnumIter, Display)]
pub enum Electrodomestico {
    Tostadora,
    Refrigeradora,
    Televisor,
    Pc,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, EnumIter, Display)]
pub enum Habitacion {
    Cocina,
    Sala,
    Dormitorio,
    Banno,
    Lavanderia,
}

impl Habitacion {
    pub fn pertenece(self, elec: Electrodomestico) -> bool {
        use Electrodomestico as E;
        match self {
            Habitacion::Dormitorio => elec == Electrodomestico::Pc,
            Habitacion::Cocina => [E::Tostadora, E::Refrigeradora].contains(&elec),
            Habitacion::Sala => elec == E::Televisor,
            _ => false,
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Calculo {
    pub cantidad: usize,
    pub potencia_referencial: usize,
    pub horas: usize,
    pub minutos: usize,
    pub dias: usize,
    pub elec: Electrodomestico,
}

impl Calculo {
    pub fn calculo(self) -> usize {
        let y = match self.elec {
            Electrodomestico::Refrigeradora => 1,
            Electrodomestico::Tostadora => 2,
            _ => 4,
        };

        y * self.horas * self.cantidad * self.dias
    }
}
