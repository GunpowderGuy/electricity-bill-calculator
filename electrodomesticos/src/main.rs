mod mis_tipos;
use mis_tipos::Calculo;
use mis_tipos::Electrodomestico;
use mis_tipos::Habitacion;
use mis_tipos::Message;

use iced::{text_input, Column, Element, Error, Radio, Row, Sandbox, Settings, Text, TextInput};

use strum::IntoEnumIterator;

const FIELDS: usize = 5;

fn main() -> Result<(), Error> {
    match State::run(Settings::default()) {
        Ok(x) => Ok(x),
        Err(e) => return Err(e),
    }
}

#[derive(Debug, Clone)]
pub struct State {
    habitacion: Habitacion,
    electrodomestico: Electrodomestico,
    espacios: [text_input::State; FIELDS],
    strings: [String; FIELDS],
    output: String,
    respuestas: Vec<Calculo>,
}

impl Sandbox for State {
    type Message = Message;

    fn title(&self) -> String {
        String::from("Counter - Iced")
    }

    fn new() -> Self {
        use std::convert::TryFrom;

        let mut vec = Vec::with_capacity(FIELDS);
        vec.resize_with(FIELDS, || "-".to_owned());
        let arr: [String; FIELDS] = <&[String; FIELDS]>::try_from(&*vec).unwrap().clone();

        State {
            habitacion: Habitacion::Dormitorio,
            electrodomestico: Electrodomestico::Refrigeradora,
            espacios: Default::default(),
            strings: arr,
            output: "aun no calculado".to_string(),
            respuestas: Default::default(),
        }
    }

    fn update(&mut self, message: Message) {
        match message {
            Message::RadioSelectedA(e) => self.habitacion = e,
            Message::RadioSelected(e) => self.electrodomestico = e,
            Message::Texto(s, u) => self.strings[u] = s,
        }

        let cantidad = self.strings[0].parse::<usize>();
        let potencia_referencial = self.strings[1].parse::<usize>();
        let horas = self.strings[2].parse::<usize>();
        let minutos = self.strings[3].parse::<usize>();
        let dias = self.strings[4].parse::<usize>();

        match (
            cantidad.clone(),
            potencia_referencial.clone(),
            horas.clone(),
            minutos.clone(),
            dias.clone(),
        ) {
            (Ok(cantidad), Ok(potencia_referencial), Ok(horas), Ok(minutos), Ok(dias)) => {
                let c = Calculo {
                    cantidad: cantidad,
                    potencia_referencial: potencia_referencial,
                    horas: horas,
                    minutos: minutos,
                    dias: dias,
                    elec: self.electrodomestico,
                };
                self.respuestas.push(c);
                self.output = c.calculo().to_string()
            }
            _ => self.output = "error de input".to_string(),
        }
    }

    fn view(&mut self) -> Element<Message> {
        //let radio_a = self.habitacion;
        let opciones_a =
            Habitacion::iter().fold(Row::new().padding(10).spacing(20), |choices, habitacion| {
                choices.push(Radio::new(
                    habitacion,
                    habitacion.to_string(),
                    Some(self.habitacion),
                    Message::RadioSelectedA,
                ))
            });

        //let radio = self.electrodomestico;
        let opciones = Electrodomestico::iter()
            .filter(|x| self.habitacion.pertenece(*x))
            .fold(
                Row::new().padding(10).spacing(20),
                |choices, electrodomestico| {
                    choices.push(Radio::new(
                        electrodomestico,
                        electrodomestico.to_string(), // revisar por que se necesita convertir
                        Some(self.electrodomestico),  // a string manualmente
                        Message::RadioSelected,
                    ))
                },
            );

        let texto_ejemplo = Text::new("Calculadora");
        let strings = &self.strings;
        let textos =
            self.espacios
                .iter_mut()
                .zip(0..FIELDS)
                .fold(Column::new(), |columna, (state, i)| {
                    columna.push(TextInput::new(state, "dfd", &strings[i], move |text| {
                        Message::Texto(text, i)
                    }))
                });

        let result = Text::new(self.output.to_string());

        let results = self.respuestas.iter().fold(
            Column::new().padding(10).spacing(20),
            |columna, calculo| {
                let result = format!(
                    "cantidad {} potencia_referencial {} horas {} minutos {} dias {}",
                    calculo.cantidad,
                    calculo.potencia_referencial,
                    calculo.horas,
                    calculo.minutos,
                    calculo.dias,
                );
                columna.push(Text::new(result))
            },
        );

        Column::new()
            .spacing(20)
            .padding(20)
            .push(texto_ejemplo)
            .push(opciones_a)
            .push(opciones)
            .push(textos)
            .push(result)
            .push(results)
            .into()
    }
}
